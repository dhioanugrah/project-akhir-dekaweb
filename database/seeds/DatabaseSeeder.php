<?php



use Illuminate\Database\Seeder;
use App\models\table;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $table_links = [
            [ 'nama' => 'Youtube', 'url' => 'www.youtube.com' ],
            [ 'nama' => 'Facebook', 'url' => 'www.Facebook.com' ],
            ['nama' => 'Instagram', 'url' => 'www.Instagram.com'],
            ['nama' => 'webtoon', 'url' => 'www.webtoon.com'],
            ['nama' => 'viu', 'url' => 'www.viu.com'],
        ];

        foreach ($table_links as $key => $value) {
            table::create([
                'nama' => $value['nama'],
                'url' => $value['url'],
            ]);
        }
    }

}
