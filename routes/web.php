<?php

use Illuminate\Support\Facades\Route;
use app\models\link;
use app\models\Informasi;
use App\Http\Controllers\LinkController;
use App\Http\Controllers\Admincontroller;
use App\Http\Controllers\GalleryController;
use App\Http\Controllers\PhotoController;
use App\Http\Controllers\InformasiController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/home', function () {
    return view('home' );

});

Route::get('/', [LinkController::class, 'index'])->name('link.index');
Route::post('/form',  [LinkController::class, 'store'])->name('store.category');
Route::get('/form/create',  [LinkController::class, 'create'])->name('link.create');
Route::get('/edit/{id}',  [LinkController::class, 'edit'])->name('link.edit');
Route::get('/delete/{id}',  [LinkController::class, 'destroy'])->name('delete.edit');
Route::get('/update/{id}',  [LinkController::class, 'update'])->name('link.update');



Route::get('/gallery', [GalleryController::class, 'index'])->name('gallery.index');
Route::post('/formgallery',  [GalleryController::class, 'store'])->name('gallery.store');
Route::get('/formgallery/create',  [GalleryController::class, 'create'])->name('gallery.create');
Route::get('/editgallery/{id}',  [GalleryController::class, 'edit'])->name('gallery.edit');
Route::get('/deletegallery/{id}',  [GalleryController::class, 'destroy'])->name('gallery.delete');
Route::put('/updategallery/{id}',  [GalleryController::class, 'update'])->name('gallery.update');

Route::get('/admin', [Admincontroller::class, 'index'])->name('admin.index')->middleware('auth');

Route::get('/informasi', [InformasiController::class, 'index'])->name('informasi.index');
Route::post('/forminformasi',  [InformasiController::class, 'store'])->name('informasi.store');
Route::get('/forminformasi/create',  [InformasiController::class, 'create'])->name('informasi.create');
Route::get('/editinformasi/{id}',  [InformasiController::class, 'edit'])->name('informasi.edit');
Route::get('/deleteinformasi/{id}',  [InformasiController::class, 'destroy'])->name('informasi.delete');
Route::put('/updateinformasi/{id}',  [InformasiController::class, 'update'])->name('informasi.update'); 

Route::get('/photo', [PhotoController::class, 'index'])->name('photo.index');
Route::post('/formphoto',  [PhotoController::class, 'store'])->name('photo.store');
Route::get('/formphoto/create',  [PhotoController::class, 'create'])->name('photo.create');
Route::get('/editphoto/{id}',  [PhotoController::class, 'edit'])->name('photo.edit');
Route::get('/deletephoto/{id}',  [PhotoController::class, 'destroy'])->name('photo.delete');
Route::put('/updatephoto/{id}',  [PhotoController::class, 'update'])->name('photo.update'); 
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');