<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class table extends Model
{
    //
    protected $table = 'table_links';
   
    protected $guarded = [
        'id','created_at','updated_at',
    ];

     
    public function perusahaans()
    {
     return $this->hasMany(perusahaan::class);   
    }
}
