<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $casts = [
        'meta'=>'object'
    ];
    protected $table = 'galleries';

    protected $guarded = [
        'id','updated_at','created_at'
    ];
}
