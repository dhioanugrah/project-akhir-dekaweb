<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Photos extends Model
{
    protected $table = "photos";
    protected $primaryKey = "id";
    protected $fillable =[ 
    'id','nama','gambar'];
}
