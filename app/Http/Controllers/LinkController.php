<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\table;
use App\models\Photos;
use App\models\Informasi;
use APp\models\gallery;

class LinkController extends Controller
{
    
    public function index()
    {
        $Informasi = Informasi ::latest()->limit(4)->get();
        $Photos = Photos ::latest()->limit(4)->get();
        $table = table::all();
        return view('data', compact('table','Photos','Informasi'));

    }
    public function create()
    {
        return view('form');
    }
    public function store(Request $request){
        
        $validateData = $request->validate([
            'nama'  => 'required',
            'url'   => 'required'
        ]);
        $link = table::create([
            'nama' => $request->nama,
            'url' => $request->url
        ]);
        return redirect()->route('link.index');
    }
    public function edit($id)
    {
        $ubah = table::findorfail($id);
        return view('edit',compact('ubah'));        
    }
    public function update(Request $request, $id)
    {
        $ubah = table::findorfail($id);
        $ubah->update($request->all());
        return redirect()->route('link.index');
    }
    public function destroy($id)
    {
        $ubah = table::findorfail($id);
        $ubah->delete();
        return back();
    }

}
