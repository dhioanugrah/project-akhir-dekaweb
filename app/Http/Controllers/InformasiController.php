<?php

namespace App\Http\Controllers;

use App\models\Informasi;
use Illuminate\Http\Request;

class InformasiController extends Controller
{
    public function index()
    {
        $Informasi = Informasi ::latest()->get();
        return view('Informasi', compact('Informasi'));

    }
    public function create()
    {
        return view('forminformasi');
    }
    public function store(Request $request)
    {
        $nm = $request->gambar;
        $namaFile = time().rand(100,999).".". $nm->GetClientOriginalName();

            $dtUpload = new Informasi;
            $dtUpload ->nama = $request->nama;
            $dtUpload->gambar = $namaFile;
            $dtUpload ->deskripsi = $request->deskripsi;

            $nm->move(public_path().'/img', $namaFile );
            $dtUpload->save();

            return redirect('photo');
    }
    public function edit($id)
    {
        $dt = Informasi::findorfail($id);
        return view('editinformasi',compact('dt'));        
    }

    public function update(Request $request, $id)
    {
        $ubah = Informasi::findorfail($id);
        $awal = $ubah->gambar;

        $dt = [
            'nama' => $request['nama'],
            'gambar' => $awal,
            'deskripsi' => $request['deskripsi']
        ];
      
        $request->gambar->move(public_path().'/img', $awal);
        $ubah->update($dt);
        return redirect('photo');
    }
    public function destroy($id)
    {
        $hapus = Informasi::findorfail($id);
        $file = public_path('/img/').$hapus->gambar;

        if(file_exists($file)){
            @unlink($file);
        }
        $hapus->delete();
        return back();
    }
}
