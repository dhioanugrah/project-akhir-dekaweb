<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\Photos;

class PhotoController extends Controller
{
    public function index()
    {
        $Photos = Photos ::latest()->get();
        return view('photo', compact('Photos'));

    }
    public function create()
    {
        return view('formphoto');
    }
    public function store(Request $request)
    {
        $nm = $request->gambar;
        $namaFile = time().rand(100,999).".". $nm->GetClientOriginalName();

            $dtUpload = new Photos;
            $dtUpload ->nama = $request->nama;
            $dtUpload->gambar = $namaFile;

            $nm->move(public_path().'/img', $namaFile );
            $dtUpload->save();

            return redirect('photo');
    }
    public function edit($id)
    {
        $dt = photos::findorfail($id);
        return view('editphoto',compact('dt'));        
    }

    public function update(Request $request, $id)
    {
        $ubah = Photos::findorfail($id);
        $awal = $ubah->gambar;

        $dt = [
            'nama' => $request['nama'],
            'gambar' => $awal,
        ];
      
        $request->gambar->move(public_path().'/img', $awal);
        $ubah->update($dt);
        return redirect('photo');
    }
    public function destroy($id)
    {
        $hapus = Photos::findorfail($id);
        $file = public_path('/img/').$hapus->gambar;

        if(file_exists($file)){
            @unlink($file);
        }
        $hapus->delete();
        return back();
    }
}
