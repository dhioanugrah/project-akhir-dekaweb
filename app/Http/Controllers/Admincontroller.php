<?php

namespace App\Http\Controllers;
use App\models\table;
use Illuminate\Http\Request;

class Admincontroller extends Controller
{
    public function index()
    {
        $table = table::all();
        return view('admin', compact('table'));

    }
}
