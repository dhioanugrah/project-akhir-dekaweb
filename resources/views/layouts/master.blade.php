<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=\, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <script src="https://kit.fontawesome.com/48470e92b4.js" crossorigin="anonymous"></script>

    <title>DeKADE CREATIVE AGENCY</title>
</head>
<style>
    html{
        scroll-behavior: smooth;
    }
     .gotopbtn{
         position: fixed;
         width: 50px;
         height: 50px;
         background: rgb(139, 133, 133);
         bottom: 10%;
         left: 1300px;

         text-decoration: none;
         text-align: center;
         line-height: 50px;
         color: white;
         font-size: 22px;
     }
</style>

<body class="bg-gray-900">
  <a class="gotopbtn" href="#"><i class="fas fa-arrow-up"></i></a>
   <nav class="bg-gray-800  mx-auto p-3 py-1">
            <div class="flex justify-between flex flex-row px-8 max-w-7x1 mx-auto text-4x2">
        <a href="/">
        <div class=" w-full sm:my-1 sm:px-1 sm:w-1/2 md:my-px md:px-px md:w-1/2 lg:my-2 lg:px-2 lg:w-1/4 xl:my-2 xl:px-2 xl:w-1/4 pb-6">
           
            <img style="max-width: 60%;height:auto;" class="" src="http://dekade.co.id/img/dekade-creative-agency.png" alt="Logo">
        </div>
    </a>

        <div class=" flex flex-row">
       
            <div class="text-white px-6 py-8 rounded-md   pr-5"> 
                
                <a href="#gallery"> GALLERY </a>   
             </div>

            <div class="text-white px-6 py-8 rounded-md   pr-4">
                <a href="#Informasi">INFORMASI</a> 
            </div>

            
           
            @if (Auth::user())
                 <div class="text-white px-6 py-8 rounded-md   pr-4  underline">
                <a href="/admin">ADMIN</a> 
            </div>
            
              <div class="text-white px-6 py-8 rounded-md   pr-4  underline dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                    LOGOUT
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </div>
           
        </div>
        @else
        <div class="text-white px-6 py-8 rounded-md   pr-4  underline">
            <a href="/login">LOGIN</a> 
        </div>
            @endif
          
        
    </div>
</nav>

@yield('content') 
    

<nav id="footer" class="bg-gray-600 py-5">
    <div class="container mx-auto pt-8 pb-4">
        <div class="flex flex-wrap overflow-hidden sm:-mx-1 md:-mx-px lg:-mx-2 xl:-mx-2">

            <div class="w-full overflow-hidden sm:my-1 sm:px-1 sm:w-1/2 md:my-px md:px-px md:w-1/2 lg:my-2 lg:px-2 lg:w-1/5 xl:my-2 xl:px-2 xl:w-1/4 pb-6">    
                <a href="http://dekade.co.id/">
                <img style="max-width: 70%;height:auto;" class="" src="http://dekade.co.id/img/dekade-creative-agency.png" alt="Logo">
                </a>
            </div>

            <div class="w-full overflow-hidden sm:my-1 sm:px-1 sm:w-1/2 md:my-px md:px-px md:w-1/2 lg:my-2 lg:px-2 lg:w-1/4 xl:my-2 xl:px-2 xl:w-1/4 pb-6">
               <h4 class="text-white">ALAMAT</h4>
                <ul class="nav navbar-nav">
                    <li id="navi-2" class="leading-7 text-sm">
                        <a href="https://www.google.co.id/maps/search/JL+Keledang+No.48+Vorvo+Samarinda+Samarinda+75123/@-0.4730549,117.1434984,17z/data=!3m1!4b1"class="text-white underline text-small ">
                            Jl. Keledang No.48, Vorfo, Samarinda
                        </a>
                </ul>


            </div>

            <div class="w-full overflow-hidden sm:my-1 sm:px-1 sm:w-1/2 md:my-px md:px-px md:w-1/2 lg:my-2 lg:px-2 lg:w-1/4 xl:my-2 xl:px-2 xl:w-1/4 pb-6">
                <h4 class="text-white">INSTAGRAM</h4>
                <ul class="">
                <li id="navi-2" class="leading-7 text-sm">
                    <a href="https://www.instagram.com/dekade.creative/?hl=id" class="text-white underline text-small">
                        dekade.creative
                     </a>
                </li>
                <li id="navi-2" class="leading-7 text-sm">
                    <a href="https://www.instagram.com/deka.de.coffee/?hl=id" class="text-white underline text-small">
                        deka.de.coffee
                     </a>
                </li>
            </div>

            <div class="w-full overflow-hidden sm:my-1 sm:px-1 sm:w-1/2 md:my-px md:px-px md:w-1/2 lg:my-2 lg:px-2 lg:w-1/4 xl:my-2 xl:px-2 xl:w-1/4 pb-6">

                <h4 class="text-white">Contact Person</h4>
                <ul class="">
                <li id="navi-2" class="leading-7 text-sm">
                    <a href="#" class="text-white underline text-small" >
                        23981830980 (GUNTUR)
                    </a>
                </li>
                <li id="navi-2" class="leading-7 text-sm">
                    <a href="#" class="text-white underline text-small" >
                        23981830980 (vivi)
                    </a>
                </li>
            </div>

         

        </div>

        <div class="pt-4 md:flex md:items-center md:justify-center " style="border-top:1px solid white"></div>
</nav>
 
    

</body>
</html>

