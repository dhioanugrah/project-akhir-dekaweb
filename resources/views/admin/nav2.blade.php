<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=\, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <title>ADMIN</title>
</head>
<body class="bg-gray-900">
  
   <nav class="bg-gray-800  mx-auto p-3 py-1">
        
    <div class="flex justify-between flex flex-row px-8 max-w-7x1 mx-auto text-4x2">
        <div class="text-white px-6 py-8 rounded-md   pr-5"> 
                
            <a href="/admin"> ADMIN </a>   
         </div>


        <div class=" flex flex-row">
       
        
             <div class="text-white px-6 py-8 rounded-md   pr-5"> 
                
                <a href="/"> HOME </a>   
             </div>
            
         
           
        </div>
        
    </div>
</nav>

@yield('content') 
    

</body>
</html>