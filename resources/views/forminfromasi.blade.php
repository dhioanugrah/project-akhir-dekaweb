<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <title>Form Gallery</title>
</head>
<body>

    <div class="min-h-screen flex items-center justify-center bg-gray-400" >

    <div class="bg-white p-10 rounded shadow-2x1 w-1/2">


        <form
        enctype="multipart/form-data"
        action= "{{ route('informasi.store') }}" method='post'>
        @csrf
            <div class="mb-8" > 
                <label class="block mb-2 font-bold ">Nama</label>
                <input type="text" id="nama" name="nama" class="w-full border-2 border-gray-300 rounded outline-none focus:border-blue-600  ">
            </div>

            <div class="mb-8" >
                <label class="block mb-2 font-bold ">file</label>
                <input type="file" id="gambar" name="gambar" class="w-full border-2 border-gray-300 rounded outline-none focus:border-blue-600  ">
            </div>

            <div class="mb-8" >
                <label class="block mb-2 font-bold ">deskripsi</label>
                <input type="description" id="deskripsi" name="deskripsi" class="w-full border-2 border-gray-300 rounded outline-none focus:border-blue-600  ">
            </div>

            <button class=" text-blue-500  px-4 py-2 rounded-md border-blue-500 border hover:bg-blue-900 hover:text-white">Tambah</button>


        </form>

    </div>
    
    </div>
</body>
</html>     