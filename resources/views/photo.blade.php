@extends('admin.nav')


@section('content')


<style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }   

            .title {
                font-size: 84px;
            }


            .m-b-md {
                margin-bottom: 30px;
            }
        
           #table {
            font-family: Arial, Helvetica, sans-serif;
         
             width: 100%;
        
            }

            #table td, #table th {
            border: 1px solid #A9A9A9;
            padding: 8px;
            color: black;
            }

            #table tr:nth-child(even){background-color: #A9A9A9;}

            #table th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #696969;
            color: white;
            }

           
        </style> 
 
 <div class=" bg-gray-600 ">              
        <div  id="gallery" class=" container max-w-10xl mx-auto m-9">
            <h2 class="w-full my-2 text-5xl font-black leading-tight text-center  text-white  ">
              GALLERY STORAGE
            </h2>
            <a href="{{ route('photo.create') }}">
                <button class="text-blue-500   px-5 py-4 rounded-md border-blue-500 border hover:bg-blue-900 hover:text-white text-right">TAMBAH GAMBAR</button>
               </a>      
               <div class="  ">
                     <table id="table" class="mb-5 bg-gray-400">
                            <thead>
                                <th>no</th>
                                <th >Nama</th>
                                <th>file</th>
                                <th>action</th>
                            </thead>
                          
                                @php
                                    $i = 1;
                                @endphp
                            
                                @foreach ( $Photos as $d )
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $d->nama }}</td>
                                    <td >
                                        <img src="{{ asset('img/'.$d->gambar) }}" class="h-40 rounded-t pb-6 bg-gray-600" alt="">
                                    </td>
                                    
                                 
                                    <td>
                                        <a href="{{ url('editphoto',$d->id) }}"  class="text-white px-4 py-2 rounded-md border-blue-500 border bg-blue-500">
                                            Edit
                                        </a>|
                                        <a href="{{ url('deletephoto',$d->id) }}" class="text-white  px-4 py-2 rounded-md border-red-500 border bg-red-500">
                                            Delete
                                        </a>       

                                    </td>
                                </tr>         
                       
                                @endforeach

                                
                        </table>
               </div>
            {{-- <div class="text-white">
                            {{ $Photos -> links() }}
                        </div> --}}
                            
                      
            </div>
        
        </div>
    </div>
@endsection